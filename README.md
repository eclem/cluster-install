# ddclient

https://doc.ubuntu-fr.org/ddclient

```bash
sudo vim /etc/ddclient.conf
```

```
protocol=dyndns2
use=web, web="ipinfo.io/ip"
server=www.ovh.com
login=clem.sh-dyn
password='58rRbSK3D3dAHV2'
dyn.clem.sh
```

```bash
sudo ddclient -daemon=0 -debug -verbose -noquiet
```

# microk8s

https://microk8s.io/docs

```bash
sudo snap install microk8s --classic
sudo usermod -a -G microk8s clem
sudo chown -f -R clem ~/.kube
```

```bash
#sudo ufw allow in on cni0 && sudo ufw allow out on cni0
#sudo ufw default allow routed
```

```bash
sudo vim /var/snap/microk8s/current/certs/csr.conf.template
```

```bash
...
DNS.6 = cluster1.clem.sh
...
```

```bash
microk8s enable dns dashboard metallb rbac storage
```
answer 10.152.183.0/24

```bash
microk8s status
microk8s config
```

```bash
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: gitlab
    namespace: kube-system
EOF
```

```bash
kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
```

```bash
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab | awk '{print $1}')
```

http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy
