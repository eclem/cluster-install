# Install K8s on a VPS

### 1. On server

##### 1.1. Prepare environnement

```bash
sudo -s
swapoff -a
apt-get -y install apt-transport-https ca-certificates software-properties-common curl
```

##### 1.2. Add docker repo

[docker install link](https://docs.docker.com/install)

debian stretch cheatsheet :

```bash
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
```

##### 1.3. Add kubernetes repo

[kubernetes install link](https://kubernetes.io/docs/setup/independent/install-kubeadm/#installing-kubeadm-kubelet-and-kubectl)

debian stretch cheatsheet :

```bash
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository "deb https://apt.kubernetes.io/ kubernetes-xenial main"
```

##### 1.4. Install bins

```bash
apt-get update
apt-get install -y docker-ce kubelet kubeadm kubernetes-cni
```

##### 1.5. Create cluster

```bash
kubeadm init --apiserver-cert-extra-sans=cluster1.eclem.net >> kubeadm_rescue.txt
```

##### 1.6. Install network

```bash
echo net.bridge.bridge-nf-call-iptables=1 >> /etc/sysctl.conf
sysctl -p
```

##### 1.7. Show kubeconfig

```bash
cat /etc/kubernetes/admin.conf
```

and save it for client

### 2. On client

##### 2.1. Install kubectl

[kubectl install link](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl)

and copy the kubeconfig in `~/.kube/config`

##### 2.2. Permit master to host pods

```bash
kubectl taint nodes --all node-role.kubernetes.io/master-
```

##### 2.3. Install network

```bash
kubectl create -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
```

##### 2.4. Install dashboard

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta6/aio/deploy/recommended.yaml
```

##### 2.5. Create admin user

```bash
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin
  namespace: kube-system
EOF
```

##### 2.6. Show admin token

```bash
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin | awk '{print $1}')
```

##### 2.7. Show API URL

```bash
kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
```

##### 2.8. Show cert

```bash
kubectl -n kube-system get secret $(kubectl -n kube-system get secret | grep admin | awk '{print $1}') -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
```

[dashboard link](http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/)
